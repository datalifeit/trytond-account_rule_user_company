# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class AccountRuleUserCompanyTestCase(ModuleTestCase):
    """Test Account Rule User Company module"""
    module = 'account_rule_user_company'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            AccountRuleUserCompanyTestCase))
    return suite
